<a href="http://yhoccotruyensaigon.com">Y HỌC CỔ TRUYỀN SÀI GÒN</a>

Bạn đang bị chứng mất ngủ đeo bám?

 Mất ngủ tìm ẩn những nguy cơ báo động

theo bạn <a href="http://yhoccotruyensaigon.com/bi-mat-ngu-co-nguy-hiem-khong-215.html">bệnh mất ngủ có nguy hiểm không</a>
Theo thống kê gần đây, Việt Nam có đến 45% số người từ độ tuổi 40 đến 60 và 75% số người từ 70 tuổi trở lên đang đối mặt với chứng mất ngủ. 
Từ khi bước sang độ trung niên, bạn thường xuyên mất ngủ, khó đi vào giấc ngủ thậm chí là cả đêm không ngủ được. Ngủ không sâu, chập chờn, dễ bị tỉnh giấc và giấc ngủ không trọn vẹn. Khi bị mất ngủ bạn càng lo lắng tìm lại giấc ngủ cho mình thì bạn lại càng rơi vào các cơn khủng hoảng và giấc ngủ lại càng khó đến.vậy bệnh mất ngủ kéo dài có sao không
Những phiền phức đầu tiên mà chứng mất ngủ mang đến là sự căng thẳng thần kinh, cảm giác ức chế khó chịu, hay cáu gắt, mất niềm vui cuộc sống, giảm tập trung trong công việc, giảm sự tỉnh táo vào ban ngày đến 32%, giảm 60% khả năng giải quyết vấn đề và đặc biệt là giảm 40% trí nhớ. 
Khi chất lượng giấc ngủ giảm sút, lão hóa sẽ đến rất nhanh, đặc biệt là ở phụ nữ. Sau 1 tuần mất ngủ, các nếp nhăn ở mặt, đuôi mắt và cổ sẽ tăng lên 45%, các vết đốm tàn nhang tăng tối thiểu 11%, không chỉ tác động xấu đến tuổi trẻ, mất ngủ triền miên của còn khiến gia tăng các bệnh lý mãn tính khác về cả sức khỏe lẫn tâm lý. Nghiêm trọng hơn, mất ngủ có thể gây thoái hóa não và dẫn đến tử vong
  
Bài thuốc chữa chứng mất ngủ của Phòng Khám Y Học Cổ Truyền Sài Gòn chính là giải pháp giúp bạn quên đi nỗi lo mất ngủ đang đeo bám, lấy lại giấc ngủ sinh lý tự nhiên. 
Công dụng của bài thuốc sẽ giúp bạn: Giúp cân bằng khí huyết, lưu thông kinh mạch, giảm chứng mất ngủ từ căn nguyên bên trong. Bổ sung các vitamin và khoáng chất, giúp bổ não lấy lại sự cân bằng. Phục hồi những thương tổn của hệ thần kinh, bệnh lạu tái phát, Giảm chứng mất ngủ, với thành phần thảo dược nên bạn không lo tác dụng phụ gây hại cho cơ thể.  Bạn sẽ có lại những giấc ngủ ngon, nhắm mắt lại và thẳng giấc cho đến lúc những tia sáng đầu tiên len lói vào phòng qua ô cửa sổ. Cuộc sống lại là những ngày nhẹ nhàng với tinh thần tỉnh táo và một sức khỏe đầy đủ. 
Thành phần bài thuốc: Sinh địa, Mạch Môn, Ngũ vị tử, Thiên môn đông, Đan sâm, Nhân sâm, Đương qui, Táo nhân, Phục linh và 1 số vị thuốc bí mật khác. 
 
 Hỗ trợ điều trị: Giúp an thần, trị mất ngủ, tim hồi hộp, mệt mỏi, khó chịu, chóng mặt, đau đầu do mất ngủ.  
Chi tiết : <a href="http://yhoccotruyensaigon.com/cach-chua-benh-mat-ngu-tai-nha-hieu-qua-bang-thuoc-dong-y-211.html">chữa bệnh mất ngủ tại nhà</a>

